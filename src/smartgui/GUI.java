/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgui;

import harjoitustyö.SQLite3;
import harjoitustyö.SmartPost;
import harjoitustyö.XMLparser;
import harjoitustyö.dataGather;
import harjoitustyö.esine;
import harjoitustyö.paketti;
import harjoitustyö.varasto;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author n8452
 */
public class GUI extends Application {
    private static varasto varasto;
    private static ArrayList<SmartPost> klista;
    private static ArrayList<esine> elista;
    private static ArrayList<paketti> plista;
    private static SQLite3 sql;

    @Override
    public void start(Stage stage) throws Exception { //listojen ja olioiden alustus controlloreita varten
        elista = new ArrayList(); 
        klista = new ArrayList();
        plista = new ArrayList(); 
        varasto = new varasto();
        sql = new SQLite3();
        varasto.päivitäesineet();   //päivitetään varastoon esineet.
        elista = varasto.getEsineet();
        plista = varasto.getPaketit();
        sql.SmartPostmaker();        //luodaan smartpost-olioita
        klista = sql.getSPList();    //oliot listaan
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    public static void päivitys () { //päivittää tietokannan
        try {
            dataGather dg = new dataGather(); //haetaan data
            XMLparser xml = new XMLparser(dg.dataGather()); //parsitaan data
            xml.getSmartPostData();         //luodaan näiden perusteella SmartPost-olioita

        } catch (IOException ex) {
        }   
    }

    public static varasto getVarasto() {
        return varasto;
    }

    public static void setVarasto(varasto varasto) {
        GUI.varasto = varasto;
    }

    public static ArrayList<SmartPost> getKlista() {
        return klista;
    }

    public static void setKlista(ArrayList<SmartPost> klista) {
        GUI.klista = klista;
    }

    public static ArrayList<esine> getElista() {
        return elista;
    }

    public static void setElista(ArrayList<esine> elista) {
        GUI.elista = elista;
    }
    public static ArrayList<paketti> getPlista() {
        return plista;
    }

    public static void setPlista(ArrayList<paketti> plista) {
        GUI.plista = plista;
    }

    public static SQLite3 getSql() {
        return sql;
    }

    public static void setSql(SQLite3 sql) {
        GUI.sql = sql;
    }

}
