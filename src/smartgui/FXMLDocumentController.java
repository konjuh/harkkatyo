/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgui;

import harjoitustyö.SQLite3;
import harjoitustyö.SmartPost;
import harjoitustyö.esine;
import harjoitustyö.paketti;
import harjoitustyö.varasto;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author n8452
 */
public class FXMLDocumentController implements Initializable {
    private ArrayList<SmartPost> klista;
    private ArrayList<paketti> plista;
    private ArrayList<esine> elista;
    private varasto varasto;
    @FXML
    private WebView WebView;
    @FXML
    private ComboBox KaupunkiCombo;
    @FXML
    private Button ButtonAction;
    @FXML
    private Button LisääKaupunkiButton;
    @FXML
    private Button UusiPakettiButton;
    @FXML
    private Button reittienTyhjennys;
    @FXML
    private ComboBox<paketti> pakettiComboBox;
    private SQLite3 sql;
    @FXML
    private Button päivitäPaketitButton;
    @FXML
    private Button LähetäPakettiButton;
    @FXML
    public void handleButtonAction(ActionEvent event) {
        GUI.päivitys();
    }  
    @Override
    public void initialize(URL url, ResourceBundle rb) { //käytettävien listojen ja olioiden alustus
        sql = new SQLite3(); 
        sql = GUI.getSql();                              //kaikki haetaan GUI:sta
        sql.SmartPostmaker();
        klista = GUI.getKlista();
        elista = GUI.getElista();
        varasto = GUI.getVarasto();
        plista = GUI.getPlista();
        WebView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        fillKComboBox();
        
    }
    @FXML
    private void LisääKartalleAutomaatit(ActionEvent event) { //hakee tietokannasta tiedot kaupungin perusteella
        String osoite,postinro,aukiolo,nimi;
        if (KaupunkiCombo.valueProperty().getValue() != null) {    
          String kaupunki = KaupunkiCombo.valueProperty().getValue().toString();
            for(int i = 0; i < klista.size(); i++) { //comboboxiin kaikki
                if (klista.get(i).getKaupunki().equals(kaupunki)) {
                    
                    osoite = klista.get(i).getOsoite();
                    osoite = osoite+",";
                    postinro = String.valueOf(klista.get(i).getPostinro());
                    nimi = klista.get(i).getNimi();
                    aukiolo = klista.get(i).getAukiolo();
                    WebView.getEngine().executeScript("document.goToLocation('"+osoite+", "+postinro+" "+kaupunki+"','"+nimi+" "+aukiolo+"','blue')");
                }            
            }
        }        
    }   
    public void fillKComboBox() { //täyttää kaupunki comboboxin kaupungeilla
        Connection conn;
        try {
            String url = "JDBC:sqlite:smartpost3.db";
            conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT DISTINCT(kaupunki) FROM osoite ORDER BY kaupunki");
            while (rs.next()) {
                String s = rs.getString("kaupunki");
                KaupunkiCombo.getItems().add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
    @FXML
    private void tyhjennäReitit(ActionEvent event) { //tyhjentää piirretyt reitit kartalta
        WebView.getEngine().executeScript("document.deletePaths()");
    }
    @FXML
    private void UusiPakettiAction(ActionEvent event) throws IOException {   //avaa uuden ikkunan, jossa voi luoda paketteja ja esineitä  
    Parent root2 = FXMLLoader.load(getClass().getResource("pakettiFXML.fxml"));
    Stage stage2 = new Stage();
    Scene scene = new Scene(root2);
    stage2.setScene(scene);
    stage2.show();  
    }
    @FXML
    public void fillpakettiComboBox () { //täyttää paketti comboboxin paketeilla
        pakettiComboBox.getItems().clear();
        for(int i = 0; i < plista.size(); i++) { //comboboxiin kaikki   
            pakettiComboBox.getItems().add(plista.get(i));
        }
    }

    @FXML
    private void LähetäPakettiAction(ActionEvent event) { //hakee automaattienID:n perusteella koordinaatit ja suorittaa piirron
        double alkulat = 0,alkulng = 0,loppulat = 0,loppulng = 0;
        ArrayList lista = new ArrayList();
        paketti paket = pakettiComboBox.valueProperty().getValue();
        
        for(int i = 0; i < klista.size(); i++) { //klista läpi 
            if (klista.get(i).getAutID() == paket.getaAutoID()) { //verrataan lähtökaupunkia ja saadaan koordinaatit näin
                alkulat = klista.get(i).getGeopoint().getLat();
                alkulng = klista.get(i).getGeopoint().getLng();
            }
        }        
        for(int i = 0; i < klista.size(); i++) { //klista läpi 
            if (klista.get(i).getAutID() == paket.getkAutoID()) { //verrataan kohdekaupunkia ja saadaan koordinaatit näin
                loppulat = klista.get(i).getGeopoint().getLat();
                loppulng = klista.get(i).getGeopoint().getLng();
            }
        }
        lista.add(alkulat);
        lista.add(alkulng);
        lista.add(loppulat);
        lista.add(loppulng);
        int luokka = paket.getLuokka();
        String väri = paket.getVäri();    
        WebView.getEngine().executeScript("document.createPath("+ lista + ", '"+väri+"',"+luokka+")"); //piirto
    }
}   

