/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgui;

import harjoitustyö.SQLite3;
import harjoitustyö.SmartPost;
import harjoitustyö.esine;
import harjoitustyö.luokka1;
import harjoitustyö.luokka2;
import harjoitustyö.luokka3;
import harjoitustyö.varasto;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konti
 */
public class PakettiFXMLController implements Initializable {
    private ArrayList<SmartPost> klista;
    private ArrayList<esine> elista;
    @FXML
    private ComboBox LähtöKaupunkiCombo;
    @FXML
    private ComboBox KohdeKaupunkiCombo;
    @FXML
    private ComboBox<SmartPost> LAutomaattiCombo;
    @FXML
    private ComboBox<SmartPost> KAutomaattiCombo;
    @FXML
    private Button PeruutaButton;
    @FXML
    private Button LuoPakettiButton;
    @FXML
    private RadioButton Luokka1button;
    @FXML
    private RadioButton Luokka2button;
    @FXML
    private RadioButton Luokka3button;
    @FXML
    private ComboBox<esine> esineComboBox;
    @FXML
    private CheckBox breakableBox;
    @FXML
    private TextField esineNimiBox;
    @FXML
    private TextField esinePainoBox;
    @FXML
    private TextField esineKokoBox;
    @FXML
    private ToggleGroup group;
    private SQLite3 sql;
    private varasto varasto;
    private luokka1 testi1;
    private luokka2 testi2;
    private luokka3 testi3;

    @FXML
    private Button LuoEsineButton;
    @FXML
    private WebView WebView;
    @FXML
    private Button infoButton;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { //alustaa listat ja oliot
        // TODO
        testi1 = new luokka1(); //alustetaan olioita, jotta voidaan tarkastella niiden ominaisuuksia (max koko jne.)
        testi2 = new luokka2();
        testi3 = new luokka3();
        
        WebView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        elista = GUI.getElista();
        varasto = GUI.getVarasto();
        klista = GUI.getKlista();
        sql = GUI.getSql();
        fillKComboBox();
        fillEsineKomboBox();
    }    
    public void fillKComboBox() { //täyttää kaupunki comboboxin kaupungeilla
        Connection conn;
        try {
            String url = "JDBC:sqlite:smartpost3.db";
            conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT DISTINCT(kaupunki) FROM osoite ORDER BY kaupunki");
            while (rs.next()) {
                String s = rs.getString("kaupunki");
                LähtöKaupunkiCombo.getItems().add(s); //molempiin comboboxeihin kaikki kaupungit
                KohdeKaupunkiCombo.getItems().add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @FXML 
    public void fillLAutoCombobox() { //täyttää kaupungin perusteella oliot lähtöcomboboxiin
        LAutomaattiCombo.getItems().clear();
        String kaupunki = LähtöKaupunkiCombo.valueProperty().getValue().toString();
        for(int i = 0; i < klista.size(); i++) { //comboboxiin kaikki
            if (klista.get(i).getKaupunki().equals(kaupunki)) {
                LAutomaattiCombo.getItems().add(klista.get(i));
            }            
        }
    } 
    @FXML
    private void fillKAutoCombobox(ActionEvent event) { //täyttää kaupungin perusteella oliot kohdecomboboxiin
        KAutomaattiCombo.getItems().clear();
        String kaupunki = KohdeKaupunkiCombo.valueProperty().getValue().toString();
        for(int i = 0; i < klista.size(); i++) { //comboboxiin kaikki
            if (klista.get(i).getKaupunki().equals(kaupunki)) {
                KAutomaattiCombo.getItems().add(klista.get(i));
            }            
        }  
    }
    @FXML
    private void PeruutaButtonAction(ActionEvent event) {
        Stage stage = (Stage) PeruutaButton.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void LuoPakettiAction(ActionEvent event) { //sisältää kaikki checkit pakettiin liittyen, ja luo lopulta paketin.
        ArrayList lista = new ArrayList();
        int c = 0; //jolla checkataan täyttyykö vaatimukset
        double matka;
        esine esine;
        try { 
            esine = esineComboBox.valueProperty().getValue();      
            if (group.getSelectedToggle() == Luokka1button ) {
                if (esine.getBool() == false) {                                              //testi, jos esine voi särkyä
                    lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                    lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                    lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                    lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                    matka = Double.parseDouble(WebView.getEngine().executeScript("document.createPath("+ lista + ", 'red', 1)").toString()); //piirto,jotta saadaan etäisyys

                    if (esineComboBox.valueProperty().getValue().getKoko() < testi1.maxkoko) { //verrataan esineen kokoa luokan max kokoon
                        c++;
                    } else {
                        System.out.println("Koko on liian suuri valitsemallesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);  //error dialogi
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Esineen koko on liian suuri valitsemallesi luokalle.");
                        alert.showAndWait(); 
                    }
                    if (esineComboBox.valueProperty().getValue().getPaino() < testi1.maxpaino) { //verrataan esineen paino luokan max painoon
                        c++;
                    } else {
                        System.out.println("Paino on liian suuri valitsemallesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Esineen paino on liian suuri valitsemallesi luokalle.");
                        alert.showAndWait(); 
                    }
                    if (matka < testi1.maxetäisyys) {           //verrataan matkan pituutta luokan max pituuteen
                        c++;
                    } else {
                        System.out.println("Matka on liian pitkä valitsemallaesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Matka on liian pitkä valitsemallesi luokalle.");
                        alert.showAndWait();
                    }
                        if (c == 3) { //jos C != 3 niin pakettia ei voida luoda, sillä jokin yllä olevista if-lauseista ei ole mennyt läpi
                            if (LAutomaattiCombo.valueProperty().getValue().getAutID() != KAutomaattiCombo.valueProperty().getValue().getAutID()) { //valittujen automaattien vertailua
                                String nimi = esineComboBox.valueProperty().getValue().toString();
                                varasto.luoLuokka1esine(esineComboBox.valueProperty().getValue(),
                                LAutomaattiCombo.valueProperty().getValue().getAutID(),KAutomaattiCombo.valueProperty().getValue().getAutID()); //luo paketin ja lisää sen varastoon 

                                sql.pakettilisays(nimi,1, LAutomaattiCombo.valueProperty().getValue().getAutID(), 
                                KAutomaattiCombo.valueProperty().getValue().getAutID());                                
                                Stage stage = (Stage) PeruutaButton.getScene().getWindow();                                                       
                                stage.close();
                            } else {
                                System.out.println("Automaatit on samoja"); //alertteja
                                Alert alert = new Alert(AlertType.ERROR);
                                alert.setTitle("Virhe");
                                alert.setHeaderText("");
                                alert.setContentText("Et voi lähettää pakettia samaan automaattiin.");
                                alert.showAndWait();  
                            }
                        } 
                } else {
                    System.out.println("Esine hajoaa älä");
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Virhe");
                    alert.setHeaderText("");
                    alert.setContentText("Esine tulee hajoamaan, ole hyvä ja valitse toinen luokka.");
                    alert.showAndWait(); 
                }
             
            } else if (group.getSelectedToggle() == Luokka2button ) {

                lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                matka = Double.parseDouble(WebView.getEngine().executeScript("document.createPath("+ lista + ", 'red', 2)").toString());
                if (esineComboBox.valueProperty().getValue().getKoko() < testi2.maxkoko) {
                    c++;
                } else {
                    System.out.println("Koko on liian suuri valitsemallesi luokalle");
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Virhe");
                    alert.setHeaderText("");
                    alert.setContentText("Esineen koko on liian suuri valitsemallesi luokalle.");
                    alert.showAndWait(); 
                }
                if (esineComboBox.valueProperty().getValue().getPaino() < testi2.maxpaino) {
                    c++;
                } else {
                    System.out.println("Paino on liian suuri valitsemallesi luokalle");
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Virhe");
                    alert.setHeaderText("");
                    alert.setContentText("Esineen paino on liian suuri valitsemallesi luokalle.");
                    alert.showAndWait(); 
                }
                if (matka < testi2.maxetäisyys) {
                    c++;
                } else {
                    System.out.println("Matka on liian pitkä valitsemallesi luokalle");
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Virhe");
                    alert.setHeaderText("");
                    alert.setContentText("Matka on liian pitkä valitsemallesi luokalle.");
                    alert.showAndWait();
                }
                if (c == 3) { //jos C != 3 niin pakettia ei voida luoda
                    if (LAutomaattiCombo.valueProperty().getValue().getAutID() != KAutomaattiCombo.valueProperty().getValue().getAutID()) { //testi onko automaatit samoja
                        String nimi = esineComboBox.valueProperty().getValue().toString();
                        varasto.luoLuokka2esine(esineComboBox.valueProperty().getValue(),
                        LAutomaattiCombo.valueProperty().getValue().getAutID(),KAutomaattiCombo.valueProperty().getValue().getAutID()); //luo paketin ja lisää sen varastoon 

                        sql.pakettilisays(nimi,2, LAutomaattiCombo.valueProperty().getValue().getAutID(), 
                        KAutomaattiCombo.valueProperty().getValue().getAutID());
                        Stage stage = (Stage) PeruutaButton.getScene().getWindow();
                        stage.close();
                    } else {
                        System.out.println("Automaatit on samoja"); //alertteja
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Et voi lähettää pakettia samaan automaattiin.");
                        alert.showAndWait();  
                    }
                }              
            }
             else if (group.getSelectedToggle() == Luokka3button) {

               if (esine.getBool() == false) {
                    lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                    lista.add(LAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                    lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLat());
                    lista.add(KAutomaattiCombo.valueProperty().getValue().getGeopoint().getLng());
                    matka = Double.parseDouble(WebView.getEngine().executeScript("document.createPath("+ lista + ", 'red', 3)").toString());
                    System.out.println(esineComboBox.valueProperty().getValue().getKoko());
                    if (esineComboBox.valueProperty().getValue().getKoko() < testi3.maxkoko) {
                        c++;
                    } else {
                        System.out.println("Koko on liian suuri valitsemallesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Esineen koko on liian suuri valitsemallesi luokalle.");
                        alert.showAndWait(); 
                    }
                    if (esineComboBox.valueProperty().getValue().getPaino() < testi3.maxpaino) {
                        c++;
                    } else {
                        System.out.println("Paino on liian suuri valitsemallesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Esineen paino on liian suuri valitsemallesi luokalle.");
                        alert.showAndWait(); 
                    }
                    System.out.println(testi3.maxetäisyys);
                    if (matka < testi3.maxetäisyys) {
                        c++;
                    } else {
                        System.out.println("Matka on liian pitkä valitsemallaesi luokalle");
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Virhe");
                        alert.setHeaderText("");
                        alert.setContentText("Matka on liian pitkä valitsemallesi luokalle.");
                        alert.showAndWait(); 
                    }
                        if (c == 3) { //jos C != 3 niin pakettia ei voida luoda
                            if (LAutomaattiCombo.valueProperty().getValue().getAutID() != KAutomaattiCombo.valueProperty().getValue().getAutID()) {
                                String nimi = esineComboBox.valueProperty().getValue().toString();
                                varasto.luoLuokka3esine(esineComboBox.valueProperty().getValue(),
                                LAutomaattiCombo.valueProperty().getValue().getAutID(),KAutomaattiCombo.valueProperty().getValue().getAutID()); //luo paketin ja lisää sen varastoon 
                                sql.pakettilisays(nimi,3, LAutomaattiCombo.valueProperty().getValue().getAutID(), 
                                KAutomaattiCombo.valueProperty().getValue().getAutID());
                                Stage stage = (Stage) PeruutaButton.getScene().getWindow();
                                stage.close();
                            } else {
                                System.out.println("Automaatit on samoja"); //alertteja
                                Alert alert = new Alert(AlertType.ERROR);
                                alert.setTitle("Virhe");
                                alert.setHeaderText("");
                                alert.setContentText("Et voi lähettää pakettia samaan automaattiin.");
                                alert.showAndWait();  
                        }
                        }
                } else {
                    System.out.println("Esine hajoaa älä");
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Virhe");
                    alert.setHeaderText("");
                    alert.setContentText("Esine tulee hajoamaan, ole hyvä ja valitse toinen luokka.");
                    alert.showAndWait(); 
            }
                
            } else {
                System.out.println("Paketin luonti epäonnistui.");
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Virhe");
                alert.setHeaderText("");
                alert.setContentText("Valitse luokka.");
                alert.showAndWait();                     
            }
        } catch (Exception e) {
            System.out.println("Paketin luonti epäonnistui.");
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Virhe");
            alert.setHeaderText("");
            alert.setContentText("Paketin luonnissa tapahtui virheitä."
                    + " Tarkasta syöttämäsi tiedot.");
            alert.showAndWait(); 
        }
}

    private void fillEsineKomboBox() { //täyttää esinecomboboxin esineillä.
        esineComboBox.getItems().clear(); //tyhjennetään ennen
        elista = varasto.getEsineet();
        for(int i = 0; i < elista.size(); i++) { //comboboxiin kaikki
            esineComboBox.getItems().add(elista.get(i));  
        }
    }
    @FXML
    private void LuoEsineAction(ActionEvent event) { //esineen luontia
        String nimi,koko;
        double paino;
        boolean breakable;
        try {
            if (breakableBox.isSelected()) {
                breakable = true;
            } else {
                breakable = false;
            }   
            nimi = esineNimiBox.getText();
            paino = Double.parseDouble(esinePainoBox.getText());
            koko = esineKokoBox.getText();
            
            sql.esineisays(nimi, koko, paino, breakable);  //lisätään tietokantaan
            varasto.luoEsine(nimi, Double.parseDouble(koko), paino, breakable); //luodaan esine olio ja lisätään varastoon
            esineComboBox.getItems().clear();
            fillEsineKomboBox();
        } catch (Exception ex) {
            System.out.println("Esineen luonti epäonnistui.");
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Virhe");
            alert.setHeaderText("");
            alert.setContentText("Esineen luonnissa tapahtui virheitä."
                    + " Tarkasta syöttämäsi tiedot.");
            alert.showAndWait(); 
        }
     }    

    @FXML
    private void infoAction(ActionEvent event) { //avaa dialogin, jossa on infoa luokista.
        Alert alert = new Alert(AlertType.INFORMATION);
    alert.setTitle("Tietoa luokista");
    alert.setHeaderText("");
    alert.setContentText("Luokan 1 painoraja: "+testi1.maxpaino+"kg kokoraja: "+testi1.maxkoko+"L. Ei särkyville esineille. \n"
            + "Luokan 2 painoraja:"+testi2.maxpaino+"kg kokoraja: "+testi2.maxkoko+"L.\n"
            + "Luokan 3 painoraja:"+testi3.maxpaino+"kg kokoraja: "+testi3.maxkoko+"L. Ei särkyville esineille.");
    alert.showAndWait();
    }
}
