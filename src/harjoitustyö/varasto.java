/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author $ JuhoK
 */
public class varasto { //pidetään kirjaa esineistä sekä paketeista

    private ArrayList<paketti> paketit = new ArrayList();
    private ArrayList<esine> esineet = new ArrayList();
    private SQLite3 sql;
    
    public void päivitäesineet () { //päivittää esineet, hakien tietokannasta
        esineet.clear();
        Connection conn = null;
        try {
            String url = "JDBC:sqlite:smartpost3.db";
            conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT * FROM esine");
            while (rs.next()) {
                String n = rs.getString("nimi");
                double s = rs.getDouble("koko");
                double w = rs.getDouble("paino");
                boolean b = rs.getBoolean("breakable");
                esine esine = new esine(n,s,w,b);
                esineet.add(esine);
            }
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    public void setPaketit(paketti p) {
        paketit.add(p);
    }
    public void luoLuokka1esine (esine esineID,int aAutoID,int kAutoID) { //ensimmäisen luokan pakettien luontiin
       luokka1 paketti = new luokka1(esineID,aAutoID,kAutoID);
       paketit.add(paketti);
    }
    public void luoLuokka2esine (esine esineID,int aAutoID,int kAutoID) { //toisen luokan pakettien luontiin
       luokka2 paketti = new luokka2(esineID,aAutoID,kAutoID);
       paketit.add(paketti);
    }
    public void luoLuokka3esine (esine esineID,int aAutoID,int kAutoID) { //kolmannen luokan pakettien luontiin
       luokka3 paketti = new luokka3(esineID,aAutoID,kAutoID);
       paketit.add(paketti);
    }
    public void luoEsine(String n,double s,double w, boolean b) {  //esineiden luontiin
        esine esine = new esine(n,s,w,b);
        esineet.add(esine);
    }
    
    public ArrayList<paketti> getPaketit() {
        return paketit;
    }                     
    public ArrayList<esine> getEsineet() {
        return esineet;
    }
}
