/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public class luokka3 extends paketti { //tää on vähän auki vielä, paketilla on luokka
    
    public luokka3(esine esine,int aAutoID,int kAutoID) {
        super(esine,aAutoID,kAutoID); 
        väri = "yellow";
        luokka = 3;
        maxetäisyys = 1500;
        maxpaino = 35;
        maxkoko = 40;
        breakable = true;

    }
    public luokka3() {
        maxetäisyys = 1500;
        maxpaino = 35;
        maxkoko = 40;
    }
    
}
