/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author $ JuhoK
 */
public class XMLparser {
        private Document doc;

     
        public XMLparser(String total) { //datan parsiminen, luodaan doc. dataGather antaa datan.
            try {
              DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
              DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
              doc = dBuilder.parse(new InputSource(new StringReader(total)));
              doc.getDocumentElement().normalize();
            } catch (ParserConfigurationException e) {
                System.err.println("Caught ParserConfigurationException!");
            } catch (IOException e) {
                System.err.println("Caught IOException!");
            } catch (SAXException e) {
                System.err.println("Caught SAXException!");
            }  

         }
        
        public void getSmartPostData() { //Datan lisäys tietokantaan
            String name,aukiolo,kaupunki,osoite;
            int autoID,postinro;
            double lat,lng;
            SQLite3 sql = new SQLite3();
            sql.deleteSPData(); //jotta ei tule duplicate tietoa poistetaan edelliset SmartPost tiedot
            NodeList nodes = doc.getElementsByTagName("item"); //lajittelua
            for(int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
         
                if (node.getNodeType() == Node.ELEMENT_NODE) { //erotellaan tiedot
                    Element e = (Element) node;               
                      name =(e.getElementsByTagName("name").item(0).getTextContent());
                      autoID = Integer.parseInt(e.getElementsByTagName("place_id").item(0).getTextContent());
                      aukiolo = (e.getElementsByTagName("availability").item(0).getTextContent());
                      kaupunki = (e.getElementsByTagName("city").item(0).getTextContent());
                      postinro = Integer.parseInt(e.getElementsByTagName("postalcode").item(0).getTextContent());
                      osoite = e.getElementsByTagName("address").item(0).getTextContent();
                      lat = Double.parseDouble(e.getElementsByTagName("lat").item(0).getTextContent());
                      lng = Double.parseDouble(e.getElementsByTagName("lng").item(0).getTextContent());
                      
                      sql.smartpostlisays(autoID, name, aukiolo); //lisätään tietokantaan automaattitietoja
                      sql.koordlisays(autoID, lat, lng); //tietokantaan koordtiedot
                      sql.osoitelisays(autoID, kaupunki, postinro, osoite); //tietokantaan osoitetiedot
                }
            }
        }

}
