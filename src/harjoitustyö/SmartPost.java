/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public class SmartPost {
    private String nimi,aukiolo,kaupunki,osoite; 
    private int autID,postinro;
    private geopoint geopoint;

        public SmartPost(String nimi, String aukiolo, String kaupunki, String osoite, 
                int postinro, int autID) {
            this.nimi=nimi;
            this.aukiolo=aukiolo;
            this.kaupunki=kaupunki;
            this.osoite=osoite;
            this.postinro=postinro;
            this.autID=autID;    
    }
        public SmartPost() {

        }
    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getAukiolo() {
        return aukiolo;
    }

    public void setAukiolo(String aukiolo) {
        this.aukiolo = aukiolo;
    }

    public String getKaupunki() {
        return kaupunki;
    }

    public void setKaupunki(String kaupunki) {
        this.kaupunki = kaupunki;
    }

    public String getOsoite() {
        return osoite;
    }

    public void setOsoite(String osoite) {
        this.osoite = osoite;
    }

    public int getPostinro() {
        return postinro;
    }

    public void setPostinro(int postinro) {
        this.postinro = postinro;
    }

    public int getAutID() {
        return autID;
    }

    public void setAutID(int autID) {
        this.autID = autID;
    }
    public geopoint getGeopoint() {
        return geopoint;
    }
    public void setGeopoint(geopoint geopoint) {
        this.geopoint = geopoint;
    }
    @Override
    public String toString() {
        return nimi;
    }
}
