/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author $ JuhoK
 */


public class SQLite3 { //Sisältää melkein kaiken tietokannasta haun ja lisäyksen.
    private ArrayList klista;
    private ArrayList lista;
    private varasto varasto;
    
    public Connection connect() {         //luodaan vain yhteys tietokantaan tällä methodilla
        Connection conn = null;
        try {
        String url = "JDBC:sqlite:smartpost3.db"; //luodaan yhteys tietokantaan jota käytämme
        conn = DriverManager.getConnection(url);

        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn; //palauttaa yhteyden sitä kutsuvalle methodille
    }   
    public void smartpostlisays(int id,String nimi,String aukiolo) { //lisätään automaattitauluja, xmlparser kutsuu pääasiassa.
        Connection conn = connect();    //yhteyden luonti tietokantaan
        try {
            PreparedStatement pstmt = conn.prepareStatement("INSERT OR REPLACE INTO automaatti(automaattiID,nimi,aukiolo)"
            + " VALUES(?,?,?)");            
            pstmt.setInt(1, id); 
            pstmt.setString(2, nimi);
            pstmt.setString(3, aukiolo);
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void koordlisays(int autID, double lat, double lng) { //lisätään koordinaattitauluja, xmlparser kutsuu pääasiassa
        Connection conn = connect();
        try {
            PreparedStatement pstmt = conn.prepareStatement("INSERT OR REPLACE"
                    + " INTO koord(automaattiID,lat,lng)"
            + " VALUES(?,?,?)");
            pstmt.setInt(1,autID);
            pstmt.setDouble(2, lat);
            pstmt.setDouble(3, lng);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void osoitelisays(int autID, String kaupunki, int postinro, String osoite) { //lisätään osoitetauluja, xmlparesr kutsuu pääasiassa
        Connection conn = connect();
        try {
            PreparedStatement pstmt = conn.prepareStatement("INSERT OR REPLACE "
                    + " INTO osoite(automaattiID,kaupunki,postinro,osoite)"
            + " VALUES(?,?,?,?)");
            pstmt.setInt(1,autID);
            pstmt.setString(2, kaupunki);
            pstmt.setInt(3, postinro);
            pstmt.setString(4, osoite);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void SmartPostmaker() { //SmartPost olioiden tekijä, hakee tietokannasta oleellisen tiedon ja luo SmartPost oliot.
        klista = new ArrayList<>();  //lista, jolle oliot tallennetaan
        String nimi,auki,koordID,kaupunki,osoite;
        int aID,postinro;
        double lat,lng;
        try {
            Connection conn = connect();
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT * FROM automaatti INNER JOIN"
                    + " osoite ON automaatti.automaattiID = osoite.automaattiID"
                    + " INNER JOIN koord ON osoite.automaattiID = koord.automaattiID");
            while (rs.next()) {
                aID = Integer.parseInt(rs.getString("automaattiID"));
                nimi = rs.getString("nimi");
                auki = rs.getString("aukiolo");
                kaupunki = rs.getString("kaupunki");
                osoite = rs.getString("osoite");
                postinro = Integer.parseInt(rs.getString("postinro"));
                lat = Float.parseFloat(rs.getString("lat"));
                lng = Float.parseFloat(rs.getString("lng"));
                geopoint gp = new geopoint(lat,lng);          //geopointin luonti
                SmartPost sp = new SmartPost(nimi,auki,kaupunki,osoite,postinro,aID); //SmartPost-olion luonti
                sp.setGeopoint(gp);                         //geopoint SmartPost-oliolle
                klista.add(sp);
            }
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteSPData() { //automaatti-,koord- ja osoitetaulujen sisällön tyhjennys päivitystä varten
        try {
            Connection conn = connect();
            Statement stmt = conn.createStatement();

            stmt = conn.createStatement();
            String sql = "DELETE FROM automaatti";
            String sql2= "DELETE FROM koord";
            String sql3= "DELETE FROM osoite";
            stmt.executeUpdate(sql);
            stmt.executeUpdate(sql2);
            stmt.executeUpdate(sql3);
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    public void esineisays(String nimi, String koko, double paino, boolean breakable) { //esine tauluun esineiden lisäystä

        Connection conn = connect();
        try {
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO esine "
            + " (nimi,koko,paino,breakable)"
            + " VALUES(?,?,?,?)");
            pstmt.setString(1,nimi);
            pstmt.setString(2,koko);
            pstmt.setDouble(3,paino);
            pstmt.setBoolean(4,breakable);
            pstmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void pakettilisays(String nimi,int luok,int aautID, int kautID) {   //paketti tauluun pakettien lisäystä
        Connection conn = connect();
        try {
            PreparedStatement pstmt = conn.prepareStatement("INSERT OR REPLACE"
                    + " INTO paketti(nimi,luokkaID,lähtöautomaattiID,loppuautomaattiID)"
            + " VALUES(?,?,?,?)");
            pstmt.setString(1, nimi);
            pstmt.setDouble(2, luok);
            pstmt.setInt(3,aautID);
            pstmt.setDouble(4, kautID);
            pstmt.executeUpdate();

            Statement stmt = conn.createStatement();
            String sql1 = "INSERT INTO lähetys(pakettiID) SELECT pakettiID from paketti"
                    + " WHERE lähtöautomaattiID= "+aautID+" AND loppuautomaattiID= "+kautID+" AND luokkaID= "+luok; //lähetys tauluun tieto paketista

            stmt.executeUpdate(sql1);
        } catch (SQLException ex) {
            Logger.getLogger(SQLite3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public ArrayList getSPList() {
        return klista;
    }

}

