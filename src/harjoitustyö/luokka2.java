/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public class luokka2 extends paketti { //tää on vähän auki vielä, paketilla on luokka
    
    public luokka2(esine esine,int aAutoID,int kAutoID) {
        super(esine,aAutoID,kAutoID); 
        väri = "blue";
        luokka = 2;
        maxetäisyys = 1500;
        maxpaino = 20;
        maxkoko = 10;
        breakable = true;

    }
    public luokka2() {
        maxetäisyys = 1500;
        maxpaino = 20;
        maxkoko = 10;
    }
    
}
