/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public abstract class paketti { //voidaan periyttää paketteja
    public int maxetäisyys;
    public double maxpaino,maxkoko;
    protected String väri;
    protected int luokka;
    protected double paino,koko;

    protected boolean breakable;

    protected int aAutoID,kAutoID;
    protected esine esine;
    
    paketti(esine esine,int aAutoID,int kAutoID) {
        this.aAutoID = aAutoID;
        this.kAutoID = kAutoID;
        this.esine = esine;
        this.paino = esine.paino;
        this.koko = esine.koko; 
        this.breakable = esine.breakable;
    }
    paketti() {
        
    }
    @Override
    public String toString() {
        return esine.getName();
    }   
    public int getaAutoID() {
        return aAutoID;
    }

    public int getkAutoID() {
        return kAutoID;
    }
    public int getLuokka() {
        return luokka;
    }
    public String getVäri() {
        return väri;
    }
}
