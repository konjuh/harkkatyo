/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public class luokka1 extends paketti { //tää on vähän auki vielä, paketilla on luokka
    
    public luokka1(esine esine,int aAutoID,int kAutoID) {
        super(esine,aAutoID,kAutoID); 
        väri = "red";
        luokka = 1;
        maxetäisyys = 150;
        maxpaino = 35;
        maxkoko = 20;
        breakable = true;

    }
    public luokka1() {
        maxetäisyys = 150;
        maxpaino = 35;
        maxkoko = 20;
    }
    public int getMaxEtäisyys() {
        return maxetäisyys;
    }
}
