/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitustyö;

/**
 *
 * @author $ JuhoK
 */
public class esine {
    public String nimi;
    public double paino,koko;
    public boolean breakable;
     
    public esine(String n,double s,double w, boolean b) {
        this.nimi = n;
        this.koko = s;
        this.paino = w;
        this.breakable = b;
    }
    public String getName() {
        return nimi;
    }
    public double getPaino() {
        return paino;
    }
    public double getKoko() {
        return koko;
    }
    public boolean getBool() {
        return breakable;
    }
    @Override
    public String toString() {
        return nimi;
    }
}
