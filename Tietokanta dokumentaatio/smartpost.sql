CREATE TABLE automaatti (
  "automaattiID" INTEGER PRIMARY KEY NOT NULL UNIQUE,
  "nimi" VARCHAR(64) NOT NULL,
  "aukiolo" VARCHAR(64) NOT NULL
  );
CREATE TABLE koord (
  "koordID" INTEGER PRIMARY KEY AUTOINCREMENT,
  "automaattiID" INTEGER NOT NULL,
  "lat" DOUBLE NOT NULL,
  "lng" DOUBLE NOT NULL,
  FOREIGN KEY ("automaattiID") REFERENCES "automaatti"("automaattiID")
  ON UPDATE CASCADE
  ON DELETE CASCADE
  );
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE osoite (
  "osoiteID" INTEGER PRIMARY KEY AUTOINCREMENT,
  "automaattiID" INTEGER NOT NULL,
  "kaupunki" VARCHAR(32),
  "postinro" INTEGER,
  "osoite" VARCHAR(64),
  FOREIGN KEY ("automaattiID") REFERENCES "automaatti"("automaattiID")
  ON UPDATE CASCADE
  ON DELETE CASCADE
  );
CREATE TABLE luokka (
  "luokkaID" INTEGER PRIMARY KEY AUTOINCREMENT,
  "kokoraja" INTEGER[][][] NOT NULL CHECK (kokoraja>0),
  "painoraja" INTEGER NOT NULL CHECK (painoraja>0),
  "maxet�isyys" INTEGER NOT NULL,
  "breakable" BOOL
  );
CREATE TABLE paketti (
"nimi" VARCHAR(64),
"pakettiID" INTEGER PRIMARY KEY AUTOINCREMENT,
"luokkaID" INTEGER NOT NULL,
 "l�ht�automaattiID" INTEGER NOT NULL,
"loppuautomaattiID" INTEGER NOT NULL,
  FOREIGN KEY ("luokkaID") REFERENCES "luokka"("luokkaID"),
FOREIGN KEY ("l�ht�automaattiID") REFERENCES "automaatti"("automaattiID"),
  FOREIGN KEY ("loppuautomaattiID") REFERENCES "automaatti"("automaattiID")
);
CREATE TABLE l�hetys (
"l�hetysID" INTEGER PRIMARY KEY AUTOINCREMENT,
"pakettiID" INTEGER NOT NULL,
"pvmr" DATE NOT NULL DEFAULT CURRENT_DATE,
FOREIGN KEY ("pakettiID") REFERENCES "paketti"("pakettiID")
);
CREATE TABLE esine (
  "esineID" INTEGER PRIMARY KEY AUTOINCREMENT,
  "nimi" VARCHAR(64),
  "koko" DOUBLE,
"paino" DOUBLE NOT NULL,
"breakable" BOOL
);